use core::borrow::{Borrow, BorrowMut};
use std::{thread, time};
use std::sync::{Arc, RwLock};

use crate::model::{ComponentDetails, Engine, Planet, World, RealObject, ObjectDetails, Vector};
use crate::model::ObjectDetails::ShipDetails;
use crate::actions::Action;

pub fn update_loop(read_writer: Arc<RwLock<World>>, action_reader: Arc<RwLock<Vec<Action>>>) {
    let mut last_time = time::Instant::now();
    let duration = time::Duration::from_millis(50);
    thread::sleep(duration);

    loop {
        let mut world: World = {
            let read_lock = read_writer.read().unwrap();
            let their_world: &World = read_lock.borrow();
            their_world.to_owned()
        };
        let actions: Vec<Action> = {
            let read_lock = action_reader.read().unwrap();
            read_lock.borrow().to_owned().to_vec()
        };
        handle_actions(world.borrow_mut(), actions);
        {
            let mut write_lock = action_reader.write().unwrap();
            let vec = write_lock.borrow_mut();
            vec.clear();
        }

        let now = time::Instant::now();
        let elapsed: time::Duration = now - last_time;
        last_time = now;

        tick(world.borrow_mut(), elapsed);
        update_world(&read_writer, world);

        thread::sleep(duration)
    }
}

fn update_world(read_writer: &Arc<RwLock<World>>, new_world: World) {
    let mut write_lock = read_writer.write().unwrap();
    let writable_world: &mut World = write_lock.borrow_mut();
    writable_world.objects = new_world.objects;
}

fn get_ship(world: &mut World) -> &mut RealObject {
    world.objects.iter_mut().find(|object| {
        match &object.details {
            ObjectDetails::ShipDetails {..} => true,
            _ => false
        }
    }).unwrap()
}

fn handle_actions(world: &mut World, actions: Vec<Action>) {
    for action in actions {
        match action {
            Action::SetVelocity {velocity, rotation} => {
                println!("Setting velocity to {:?}, {:?}", velocity, rotation);
                let ship = get_ship(world);
                let radians = (rotation as f64).to_radians();
                let actual_velocity = velocity * 10.0;
                let velocity_vector = Vector {
                    x: actual_velocity * radians.cos(),
                    y: actual_velocity * radians.sin()
                };
                ship.kinematic_state.velocity = velocity_vector;
            },
            Action::SetThrust {thrust, rotation} => {
                let desired_thrust = thrust;
                let radians = (rotation as f64).to_radians();
                let ship = get_ship(world);
                if let ShipDetails(ref mut ship) = ship.details {
                    ship.components.iter_mut().for_each(|component| {
                        match component.details {
                            ComponentDetails::EngineDetails(ref mut engine) => {
                                let direction_vector = Vector {
                                    x: radians.cos(),
                                    y: radians.sin()
                                };
                                engine.thrust = (desired_thrust * 10.0) as i32;
                                engine.rotation = direction_vector;
                            }
                        }
                    });
                }

            },
            Action::Reset => {
                world.objects = World::test_instance().objects;
            }
        }
    }
}

fn tick(world: &mut World, elapsed: time::Duration) {
    let elapsed_millis = elapsed.as_millis();
    let elapsed_millis_f = elapsed_millis as f64;
    let elapsed_sec_f = elapsed_millis_f / 1000.0;

    let planets = world.planets();
    for object in &mut world.objects {
        let gravity: Vector = net_gravity(object, &planets);
        let thrust = net_thrust(object);

        object.transform.position += &object.kinematic_state.velocity * elapsed_sec_f;
        object.kinematic_state.velocity += (gravity + thrust) * elapsed_sec_f;
    }
}

fn net_gravity(object: &RealObject, planets: &Vec<(RealObject, Planet)>) -> Vector {
    planets.iter().fold(Vector::empty(), |sum, (planet_stats, planet)| {
        let distance: Vector = &planet_stats.transform.position - &object.transform.position;
        let distance_sqrd: f64 = distance.scalar_sqrd();

        let result = if distance_sqrd > 0.0 {
            let magnitude = planet.mass / distance_sqrd;
            sum + (distance.normalized() * magnitude)
        } else {
            sum
        };
        result
    })
}

fn net_thrust(object: &RealObject) -> Vector {
    match &object.details {
        ObjectDetails::ShipDetails(ship) => {
            ship.components.iter().fold(Vector::empty(), |net, component| {
                match &component.details {
                    ComponentDetails::EngineDetails(Engine {thrust, rotation}) => net + (rotation * (*thrust as f64))
                }
            })
        }
        _ => Vector::empty()
    }
}