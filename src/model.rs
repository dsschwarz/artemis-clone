use std::f64::consts::PI;

use serde::{Deserialize, Serialize};

use core::ops::{Add, AddAssign, Sub, Mul};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct World {
    pub objects: Vec<RealObject>
}

impl World {

    pub fn test_instance() -> World {
        let mut objects: Vec<RealObject> = Vec::new();
        objects.push(RealObject {
            shape: Shape {
                width: 10,
                height: 10,
                offset_x: 0,
                offset_y: 0
            },
            transform: Transform {
                position: Vector {x: 200.0, y: 100.0},
                rotation: Direction { radians: 0.0 }
            },
            kinematic_state: KinematicState {
                velocity: Vector::empty(),
                angular_velocity: 0.0
            },
            details: ObjectDetails::PlanetDetails(
                Planet {
                    mass: 1000.0
                }
            )
        });

        {
            let mut components: Vec<Component> = Vec::new();
            components.push(Component {
                location: Vector::empty(),
                details: ComponentDetails::EngineDetails(Engine {rotation: (Vector {x: 0.0, y: 1.0}).normalized(),  thrust: 0})
            });
            objects.push(RealObject {
                shape: Shape {
                    width: 5,
                    height: 3,
                    offset_x: 0,
                    offset_y: 0
                },
                transform: Transform {
                    position: Vector {x: 200.0, y: 150.0},
                    rotation: Direction {radians: PI/4.0}
                },
                kinematic_state: KinematicState {
                    velocity: Vector {x: 5.0, y: 0.0},
                    angular_velocity: -1.0
                },
                details: ObjectDetails::ShipDetails(Ship {
                    id: 0,
                    components
                })
            });
        }

        return World{objects};
    }

    pub fn planets(&self) -> Vec<(RealObject, Planet)> {
        let mut planet_col: Vec<(RealObject, Planet)> = vec![];
        for obj in &self.objects {
            let details: &ObjectDetails = &obj.details;
            match details {
                ObjectDetails::PlanetDetails(planet) => planet_col.push((obj.clone(), planet.clone())),
                _ => (),
            }
        }
        planet_col
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct RealObject {
    pub shape: Shape,
    pub transform: Transform,
    pub kinematic_state: KinematicState,
    pub details: ObjectDetails
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Shape {
    pub width: i32,
    pub height: i32,
    pub offset_x: i32,
    pub offset_y: i32
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Transform {
    pub position: Vector,
    pub rotation: Direction
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Vector {
    pub x: f64,
    pub y: f64
}

impl Add<Vector> for Vector {

    type Output = Vector;

    fn add(self, other: Vector) -> Vector {
        &self + other
    }
}

impl<'a> Add<Vector> for &'a Vector {

    type Output = Vector;

    fn add(self, other: Vector) -> Vector {
        Vector {
            x: self.x + other.x,
            y: self.y + other.y
        }
    }
}

impl AddAssign<Vector> for Vector {
    fn add_assign(&mut self, rhs: Vector) {
        *self = self.clone() + rhs;
    }
}

impl Sub<Vector> for Vector {

    type Output = Vector;

    fn sub(self, other: Vector) -> Vector {
        &self - &other
    }
}

impl<'a> Sub<Vector> for &'a Vector {

    type Output = Vector;

    fn sub(self, other: Vector) -> Vector {
        self - &other
    }
}

impl<'a, 'b> Sub<&'b Vector> for &'a Vector {

    type Output = Vector;

    fn sub(self, other: &'b Vector) -> Vector {
        Vector {
            x: self.x - other.x,
            y: self.y - other.y
        }
    }
}

impl Mul<f64> for Vector {

    type Output = Vector;

    fn mul(self, rhs: f64) -> Self::Output {
        &self * rhs
    }
}

impl<'a> Mul<f64> for &'a Vector {

    type Output = Vector;

    fn mul(self, rhs: f64) -> Self::Output {
        Vector {
            x: self.x * rhs,
            y: self.y * rhs
        }
    }
}

impl Vector {

    pub fn normalized(&self) -> Vector {
        let total: f64 = (self.x*self.x + self.y*self.y).sqrt();
        Vector {
            x: self.x/total,
            y: self.y/total
        }
    }

    pub fn scalar_sqrd(&self) -> f64 {
        self.x*self.x + self.y*self.y
    }

    pub fn empty() -> Vector {
        return Vector{x:0.0, y:0.0};
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Direction {
    pub radians: f64
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct KinematicState {
    pub velocity: Vector, // pixels per second
    pub angular_velocity: f64 // degrees per second
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Planet {
    pub mass: f64
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub enum ObjectDetails {
    PlanetDetails(Planet),
    ShipDetails(Ship)
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Ship {
    pub id: i32,
    pub components: Vec<Component>
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Component {
    pub location: Vector,
    pub details: ComponentDetails
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub enum ComponentDetails {
    EngineDetails(Engine)
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Engine {
    pub thrust: i32,
    pub rotation: Vector
}