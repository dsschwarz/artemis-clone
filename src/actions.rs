
#[derive(Clone, Debug)]
pub enum Action {
    SetVelocity {
        velocity: f64,
        rotation: i64 // in degrees
    },
    SetThrust {
        thrust: f64,
        rotation: i64 // in degrees
    },
    Reset
}