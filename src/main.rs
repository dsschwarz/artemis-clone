#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;

use core::borrow::{Borrow, BorrowMut};
use std::{thread};
use std::io::Error;
use std::sync::{Arc, RwLock};

use rocket::{State};
use rocket::response::NamedFile;
use rocket_contrib::serve::StaticFiles;
use serde_json;

use model::World;
use updater::update_loop;

use crate::actions::Action;

mod model;
mod updater;
mod actions;

#[get("/")]
fn index() -> Result<NamedFile, Error> {
    return NamedFile::open("html/game.html");
}

#[get("/gameState")]
fn get_game_state(state: State<Arc<RwLock<World>>>) -> serde_json::Result<String> {
    let read_lock = state.borrow().read().unwrap();
    let world: &World  = read_lock.borrow();
    return serde_json::to_string(world);
}

#[post("/action", data = "<action>")]
fn handle_action(state: State<Arc<RwLock<Vec<Action>>>>, action: String) -> serde_json::Result<String> {
    let v: serde_json::Value = serde_json::from_str(&action)?;

    let action_type = v["type"].as_str().unwrap();
    let parsed_action = match action_type.as_ref() {
        "setVelocity" => Action::SetVelocity {
            velocity: v["velocity"].as_f64().unwrap(),
            rotation: v["rotation"].as_i64().unwrap()
        },
        "setThrust" => Action::SetThrust {
            thrust: v["thrust"].as_f64().unwrap(),
            rotation: v["rotation"].as_i64().unwrap()
        },
        "reset" => Action::Reset,
        _ => {
            panic!("Unrecognized action type {:}", action_type)
        }
    };

    {
        let mut write_lock = state.borrow().write().unwrap();
        let vec: &mut Vec<Action> = write_lock.borrow_mut();
        vec.push(parsed_action);
    }

    std::result::Result::Ok(action)
}

fn main() {
    let initial_world: World = World::test_instance();

    let read_writer: RwLock<World> = RwLock::new(initial_world);

    let reference_count = Arc::new(read_writer);
    let actions: Arc<RwLock<Vec<Action>>> = Arc::new(RwLock::new(Vec::new()));
    spawn_updater(Arc::clone(&reference_count), Arc::clone(&actions));
    rocket::ignite()
        .mount("/", routes![index, get_game_state, handle_action])
        .mount("/public", StaticFiles::from("./public"))
        .manage(Arc::clone(&reference_count))
        .manage(Arc::clone(&actions))
        .launch();
}

fn spawn_updater(read_writer: Arc<RwLock<World>>, action_reader: Arc<RwLock<Vec<Action>>>) {
    thread::spawn(move || {
        update_loop(read_writer, action_reader);
    });
}
