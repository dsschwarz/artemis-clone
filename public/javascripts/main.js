import {gameReducer, GameUI} from "./components/gameUI.js";
import {RenderEngine} from "./renderEngine.js";
import {Networking} from "./network.js";

function initializeUI() {
    let container = $('#game-container');

    let store = Redux.createStore(gameReducer);

    window.gameUI = new GameUI(store);

    let viewModel = createViewModel();

    ko.applyBindings(viewModel);

    let canvas = $("#mainCanvas")[0];
    let renderEngine = new RenderEngine(canvas.getContext("2d"));

    store.subscribe(function () {
        updateViewModel(viewModel, store.getState());
        renderEngine.setGameState(store.getState().gameState);
        renderEngine.render();
    });

    poll(store);
}

function createViewModel(store) {
    return {
    };
}

function updateViewModel(viewModel, state) {

}

function poll(store) {
    Networking.getGameState().done(function (gameState) {
        store.dispatch({
            type: "updateGameState",
            gameState: JSON.parse(gameState)
        });

        setTimeout(function () {
            poll(store);
        }, 300);
    });
}

$(function () {
    initializeUI();
});