export class RenderEngine {
    HEIGHT = 800;
    WIDTH = 1000;

    constructor(context) {
        this.gameState = null;
        this.context = context;
    }

    setGameState(newState) {
        this.gameState = newState;
    }

    /**
     * @param context
     */
    render() {
        var context = this.context;
        context.clearRect(0, 0, this.WIDTH, this.HEIGHT);

        this.drawBackground();

        this.drawObjects();
    }

    drawBackground() {
        this.context.beginPath();
        this.context.fillStyle = "black";
        this.context.rect(0, 0, this.WIDTH, this.HEIGHT);
        this.context.fill();
    }

    drawObjects() {
        var that = this;
        if (that.gameState) {
            that.gameState.objects.forEach(function (realObject) {
                var position = realObject.transform.position;
                var shape = realObject.shape;
                that.context.beginPath();
                that.context.fillStyle = "brown";
                that.context.rect(position.x, position.y, shape.width, shape.height);
                that.context.fill();
            });
        }
    }
}
