export class Networking {

    static sendAction(action) {
        return $.post("/action", JSON.stringify(action));
    }
    static getGameState() {
        return $.get("/gameState");
    }
}