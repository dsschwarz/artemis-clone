import {Networking} from "../network.js";
import {Actions} from "../actions.js";


var initialState = {
    gameState: null
};

export class GameUI {

    _store;
    constructor(store) {
        this._store = store;
    }

    clickSetThrust() {
        this._store.dispatch(Actions.setThrust(this._getMagnitude(), this._getRotation(), ""))
    }

    clickSetVelocity() {
        this._store.dispatch(Actions.setVelocity(this._getMagnitude(), this._getRotation(), ""))
    }

    clickReset() {
        this._store.dispatch(Actions.reset());
    }

    _getMagnitude() {
        return parseFloat( $("#magnitudeSlider").val());
    }
    _getRotation() {
        return parseFloat( $("#rotationSlider").val());
    }
}

export var gameReducer = createReducer(initialState, {
    "updateGameState": function (state, action) {
        return {
            gameState: action.gameState
        }
    },
    "setVelocity": function (state, action) {
        Networking.sendAction(action);
        return {};
    },
    "setThrust": function (state, action) {
        Networking.sendAction(action);
        return {};
    },
    "reset": function (state, action) {
        Networking.sendAction(action);
        return {};
    }
});

function createReducer(initialState, handlers) {
    return function reducer(state = initialState, action) {
        if (handlers.hasOwnProperty(action.type)) {
            var stateOverrides = handlers[action.type](state, action);
            return {...state, ...stateOverrides};
        } else {
            return state
        }
    }
}