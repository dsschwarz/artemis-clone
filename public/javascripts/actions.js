export class Actions {

    static setVelocity(velocity, rotation, id) {
        return {
            type: "setVelocity",
            velocity,
            rotation,
            id
        }
    }

    static setThrust(thrust, rotation, id) {
        return {
            type: "setThrust",
            thrust,
            rotation,
            id
        }
    }

    static reset() {
        return {
            type: "reset"
        }
    }
}